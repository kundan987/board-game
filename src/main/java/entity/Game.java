package entity;

import enums.GameStatus;

public class Game {

    private Player[] players;

    private Board board;

    private Player playerTurn;

    private GameStatus status;

    public Game(Player[] players, int row, int col, Player startPlayer) {
        this.players = players;
        this.board = new Board();
        board.initialize(row, col);
        this.playerTurn = startPlayer;
        this.status = GameStatus.ACTIVE;
    }


    public boolean makeMove(Player player1, int x, int y) {

        if(isValidMove(x,y)){
            System.out.println("place player char at box");
            return true;

        }
        return false;
    }

    public boolean checkStatus() {
        System.out.println("algo to check win for current player");
        System.out.println();
        return false;
    }

    private boolean isValidMove(int x, int y) {
        //box is empty
        System.out.println("Validating the move");
        return true;
    }
}
