package entity;

public class Board {

    Box[][] board;

    private void board(int row, int col) {
        this.board = new Box[row][col];
        for(int i=0;i<row;i++)
            for(int j=0; j<col; j++)
                this.board[i][j] = new Box(i,j);

    }

    public void initialize(int row, int col){
        board(row, col);
    }
}
