package entity;

public class Player extends Identity{

    private char character;
    private boolean isComputer;

    public Player(char character, boolean isComputer) {
        this.character = character;
        this.isComputer = isComputer;
    }

    public char getCharacter() {
        return character;
    }

    public void setCharacter(char character) {
        this.character = character;
    }

    public boolean isComputer() {
        return isComputer;
    }

    public void setComputer(boolean computer) {
        isComputer = computer;
    }
}
