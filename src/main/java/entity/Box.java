package entity;

public class Box {

    int x;
    int y;
    Character character;

    public Box() {
    }

    public Box(int x, int y) {
        this.x = x;
        this.y = y;
        this.character = null;
    }
}
