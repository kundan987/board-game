package service;

import entity.Game;
import entity.Player;

public class GameService {

    public static void main(String[] args) {
        Player player1 = new Player('X', false);
        Player player2 = new Player('O', false);
        Game game = new Game(new Player[]{player1, player2}, 3, 3, player1);
        boolean flag = true;
        while (game.checkStatus()){
            if(flag){
                game.makeMove(player1, 1, 2);

            } else {
                game.makeMove(player2, 1, 2);
            }
            flag = !flag;

        }
    }
}
