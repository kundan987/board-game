package enums;

public enum GameStatus {

    ACTIVE, CANCELED, DONE
}
